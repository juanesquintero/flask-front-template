
from .flask_base import client, get_html

def test_home_index():
    res = client.get('/')
    assert res.status_code == 200
    assert 'text/html' in res.headers['Content-Type']  

    res = client.post('/')
    assert res.status_code == 400
    assert 'text/html' in res.headers['Content-Type']  
    
    html = get_html(res)
    div = html.find('div', {'class': 'text-center'})

    h1 = div.find('h1', recursive=False)
    assert h1.text == '400 - Bad Request'
    p = div.find('p', recursive=False)
    assert p.text == 'The CSRF token is missing..'


def test_home_api_call():
    response = client.get('/api-call')
    assert response.status_code == 200
    
    html = get_html(response)
    p = html.find('p', {'id': 'api-response'})
    assert p.text.strip() == "{'api': 'SEDIC'}"


