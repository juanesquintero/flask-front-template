from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

USERS = []

class User(UserMixin):

    def __init__(self, id=None, name=None, password='', is_admin=False):
        self.id = id
        self.name = name
        self.password = generate_password_hash(password)
        self.is_admin = is_admin

    @staticmethod
    def exists(user_id, login=False):
        func_id = lambda u: u.id == user_id
        users_filtered = list(filter(func_id, USERS))
        if users_filtered:
            return users_filtered[0]
        return None

    def get(self, user_id):
        user = self.exists(user_id)
        return user

    def save(self):
        exists = self.exists(self.id)
        if not exists:
            USERS.append(self)
            return True
        return False

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<User {}>'.format(self.name)
